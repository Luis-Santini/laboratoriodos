package com.santini.laboratorio02

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnRegistrar.setOnClickListener {
            var nombre = edtNombre.text.toString()
            var edad: String = edtEdad.text.toString()
            var tipoAnimal: String
            var checkSelection = 0
            var resultVacuna: String = ""
            if (nombre.isEmpty()) {
                Toast.makeText(this, "Debe ingresar el nombre", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (edad.isEmpty()) {
                Toast.makeText(this, "Debe ingresar la edad", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (rdPerro.isChecked) {
                tipoAnimal = "Perro"
            } else if (rdGato.isChecked) {
                tipoAnimal = "Gato"
            } else {
                tipoAnimal = "Conejo"
            }
            if (chkAntiparasital.isChecked) {
                resultVacuna += " - Antiparasital \n"
                checkSelection = 1
            }
            if (chkAntirabica.isChecked) {
                resultVacuna += " - Antirabica \n"
                checkSelection = 1
            }
            if (chkHepatitis.isChecked) {
                resultVacuna += " - Hepatitis \n"
                checkSelection = 1
            }
            if (chkMoquillo.isChecked) {
                resultVacuna += " - Moquillo \n"
                checkSelection = 1
            }
            if (chkParainfluenza.isChecked) {
                resultVacuna += " - Parainfluenza \n"
                checkSelection = 1
            }
            if (chkParvovirus.isChecked) {
                resultVacuna += " - Parvovirus \n"
                checkSelection = 1
            }
            if (checkSelection == 0) {
                resultVacuna = " No cuenta con ninguna vacuna \n"
            }
            var bundle = Bundle()
            bundle.apply {
                putString("Key_nombre", nombre)
                putString("key_edad", edad)
                putString("key_tipoAnimal", tipoAnimal)
                putString("key_vacunas", resultVacuna)
            }
            val intent = Intent(this, ResumenActivity::class.java)
            intent.putExtras(bundle)
            startActivity(intent)
        }
    }
}