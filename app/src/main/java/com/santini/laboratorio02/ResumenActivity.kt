package com.santini.laboratorio02

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import kotlinx.android.synthetic.main.activity_resumen.*

class ResumenActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_resumen)
        recuperarValoresMainActivity()
    }

    fun recuperarValoresMainActivity() {
        var bundle = intent.extras
        var tipoAnimal = bundle!!.getString("key_tipoAnimal")
        if (tipoAnimal == "Perro") {
            imgTipoAnimal.setImageResource(R.drawable.perro)
        } else if (tipoAnimal == "Gato") {
            imgTipoAnimal.setImageResource(R.drawable.gato)
        } else {
            imgTipoAnimal.setImageResource(R.drawable.conejo)
        }
        tvNombre.text = bundle!!.getString("Key_nombre")
        tvEdad.text = bundle!!.getString("key_edad")
        tvTipoAnimal.text = tipoAnimal
        tvVacunas.text = bundle!!.getString("key_vacunas")
    }
}


